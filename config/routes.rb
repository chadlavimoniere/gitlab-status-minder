Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'users/registrations' }

  resources :statuses
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  get "about" => "pages#about"

  authenticated :user do
    root to: "statuses#index"
  end

  unauthenticated :user do
    root to: "pages#home", as: :unauthenticated_root
  end

  # Defines the root path route ("/")
end
