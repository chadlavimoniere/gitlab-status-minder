# EMOJIS = [
#   ['🌴', 'palm_tree'],
#   ['💬', 'speech_balloon'],
#   ['☕️', 'coffee'],
# ].freeze

# There are some emojis here that don't work, need to find them an exclude them
EMOJIS = Emoji.all.filter{ |e| e.name != 'dotted_line_face'}.map { |e| ["#{e.name} #{e.raw}", e.name]}