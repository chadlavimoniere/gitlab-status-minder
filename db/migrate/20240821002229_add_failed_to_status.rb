class AddFailedToStatus < ActiveRecord::Migration[7.1]
  def change
    add_column :statuses, :failed, :boolean
  end
end
