class AddDoneToStatuses < ActiveRecord::Migration[7.1]
  def change
    add_column :statuses, :done, :boolean
  end
end
