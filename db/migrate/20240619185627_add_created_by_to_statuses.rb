class AddCreatedByToStatuses < ActiveRecord::Migration[7.1]
  def change
    add_column :statuses, :created_by, :integer
  end
end
