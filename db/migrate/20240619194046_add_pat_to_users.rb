class AddPatToUsers < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :personal_access_token, :string
  end
end
