class CreateStatuses < ActiveRecord::Migration[7.1]
  def change
    create_table :statuses do |t|
      t.string :text
      t.boolean :busy
      t.string :emoji
      t.datetime :starts
      t.datetime :expires

      t.timestamps
    end
  end
end
