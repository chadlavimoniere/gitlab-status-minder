class UpdateStatusJob < ApplicationJob
  queue_as :default

  def perform(status)
    return if status.posted?

    return if status.starts > Time.current

    status.api_call!
  end
end
