class ClearStatusJob < ApplicationJob
  queue_as :default

  def perform(status)
    return if status.expires.nil?
    
    return if status.expires > Time.current
    
    return if !status.posted?

    status.clear_status
  end
end
