module ApplicationHelper
  def format_date_time(date_time)
    date_time.in_time_zone(current_user.time_zone).strftime("%F %l:%M %p")
  end

  ONE_MINUTE = (1.0/24/60)
  
  def one_minute_from_now_datetime
    DateTime.current + ONE_MINUTE
  end
  def one_minute_from_now_date
    Date.current + ONE_MINUTE
  end

  def get_emoji_by_name(name)
    result = Emoji.find_by_alias(name)
    result.raw if result
  end

  def get_current_status()
    JSON.parse Faraday.new(
      url: "https://gitlab.com/api/v4/user/status",
      params: {},
      headers: {'PRIVATE-TOKEN' => current_user.personal_access_token}
    ).get().body
  end
end
