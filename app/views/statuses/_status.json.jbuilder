json.extract! status, :id, :text, :busy, :emoji, :starts, :expires, :created_at, :updated_at
json.url status_url(status, format: :json)
