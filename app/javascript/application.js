// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails";
import "controllers";
import EmojiPicker from "controllers/emojiPicker";

const createTimelineStyles = (selector) => {
  const parent = document.querySelector(selector);
  if (parent) {
    const topOffset =
      parent.querySelector("li:first-child").getBoundingClientRect().height / 2;
    const bottomOffset =
      parent.querySelector("li:last-child").getBoundingClientRect().height / 2;
    const statusesHeight = parent.getBoundingClientRect().height;
    const sheet = document.styleSheets[0];

    sheet.insertRule(
      `${selector}:before { height: ${
        statusesHeight - topOffset - bottomOffset
      }px !important; margin-top: ${topOffset}px; }`,
      sheet.rules.length
    );
    parent.classList.add("with-timelines");
  }
};

const fixStatusesTimeline = () => {
  if (["/", "/statuses"].includes(window.location.pathname)) {
    createTimelineStyles("#statuses");
    createTimelineStyles("#past-statuses");
    window.addEventListener("resize", fixStatusesTimeline);
  } else {
    window.removeEventListener("resize", fixStatusesTimeline);
  }
};

document.addEventListener("turbo:render", fixStatusesTimeline);
document.addEventListener("DOMContentLoaded", fixStatusesTimeline);

const setUpEmojiPicker = () => {
  new EmojiPicker({
    selector: "#emoji-picker-trigger",
    insertInto: "#emoji-input",
  });
};

document.addEventListener("turbo:load", setUpEmojiPicker);
