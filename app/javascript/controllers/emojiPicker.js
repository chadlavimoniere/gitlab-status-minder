import emojiData from "controllers/emojiData";

const EmojiPicker = function (options) {
  this.options = options;
  this.trigger = this.options.selector;
  this.insertInto = undefined;
  let emojisHTML = "";
  let categoriesHTML = "";
  let categoryList = undefined;
  let emojiList = undefined;
  let mouseMove = false;
  const pickerWidth = 350;
  const pickerHeight = 400;

  this.lib = function (el = undefined) {
    const isNodeList = (nodes) => {
      var stringRepr = Object.prototype.toString.call(nodes);

      return (
        typeof nodes === "object" &&
        /^\[object (HTMLCollection|NodeList|Object)\]$/.test(stringRepr) &&
        typeof nodes.length === "number" &&
        (nodes.length === 0 ||
          (typeof nodes[0] === "object" && nodes[0].nodeType > 0))
      );
    };

    return {
      el: () => {
        // Check if is node
        if (!el) {
          return undefined;
        } else if (el.nodeName) {
          return [el];
        } else if (isNodeList(el)) {
          return Array.from(el);
        } else if (typeof el === "string" || typeof el === "STRING") {
          return Array.from(document.querySelectorAll(el));
        } else {
          return undefined;
        }
      },

      on(event, callback, classList = undefined) {
        if (!classList) {
          this.el().forEach((item) => {
            item.addEventListener(event, callback.bind(item));
          });
        } else {
          this.el().forEach((item) => {
            item.addEventListener(event, (e) => {
              if (e.target.closest(classList)) {
                let attr = undefined;

                if (Array.isArray(classList)) {
                  const stringifiedElem = e.target.outerHTML;

                  const index = classList.findIndex((attr) =>
                    stringifiedElem.includes(attr.slice(1))
                  );

                  attr = classList[index];
                }

                callback(e, attr);
              }
            });
          });
        }
      },

      css(params) {
        for (const key in params) {
          if (Object.hasOwnProperty.call(params, key)) {
            const cssVal = params[key];
            this.el().forEach((el) => (el.style[key] = cssVal));
          }
        }
      },

      attr(param1, param2 = undefined) {
        if (!param2) {
          return this.el()[0].getAttribute(param1);
        }
        this.el().forEach((el) => el.setAttribute(param1, param2));
      },

      removeAttr(param) {
        this.el().forEach((el) => el.removeAttribute(param));
      },

      addClass(param) {
        this.el().forEach((el) => el.classList.add(param));
      },

      removeClass(param) {
        this.el().forEach((el) => el.classList.remove(param));
      },

      slug(str) {
        return str
          .toLowerCase()
          .replace(/[^\u00BF-\u1FFF\u2C00-\uD7FF\w]+|[\_]+/gi, "-")
          .replace(/ +/g, "-");
      },

      remove(param) {
        this.el().forEach((el) => el.remove());
      },

      val(param = undefined) {
        let val;

        if (param === undefined) {
          this.el().forEach((el) => {
            val = el.value;
          });
        } else {
          this.el().forEach((el) => {
            el.value = param;
          });
        }

        return val;
      },

      text(msg = undefined) {
        if (msg === undefined) {
          return el.innerText;
        } else {
          this.el().forEach((el) => {
            el.innerText = msg;
          });
        }
      },

      html(data = undefined) {
        if (data === undefined) {
          return el.innerHTML;
        } else {
          this.el().forEach((el) => {
            el.innerHTML = data;
          });
        }
      },
    };
  };

  // GitLab can't support emojis with unicode version greater than 9.0
  const emojiObj = emojiData.filter(
    (emoji) => parseFloat(emoji.unicode_version) <= 9
  );

  const icons = {
    search:
      '<svg class="w-4 h-4" style="fill: #646772;" version="1.1" width="17" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 487.95 487.95" style="enable-background:new 0 0 487.95 487.95;" xml:space="preserve"> <g> <g> <path d="M481.8,453l-140-140.1c27.6-33.1,44.2-75.4,44.2-121.6C386,85.9,299.5,0.2,193.1,0.2S0,86,0,191.4s86.5,191.1,192.9,191.1 c45.2,0,86.8-15.5,119.8-41.4l140.5,140.5c8.2,8.2,20.4,8.2,28.6,0C490,473.4,490,461.2,481.8,453z M41,191.4 c0-82.8,68.2-150.1,151.9-150.1s151.9,67.3,151.9,150.1s-68.2,150.1-151.9,150.1S41,274.1,41,191.4z"/> </g> </g> <g> </g> <g> </g> </svg>',
    move: '<svg class="w-4 h-4 fill-current" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512.006 512.006" xml:space="preserve"> <g> <g> <path d="M508.247,246.756l-72.457-72.465c-5.009-5.009-13.107-5.009-18.116,0c-5.009,5.009-5.009,13.107,0,18.116l50.594,50.594 H268.811V43.748l50.594,50.594c5.009,5.009,13.107,5.009,18.116,0c5.009-5.009,5.009-13.107,0-18.116L265.056,3.761 c-5.001-5.009-13.107-5.009-18.116,0l-72.457,72.457c-5.009,5.009-5.009,13.107,0,18.116c5.001,5.009,13.107,5.009,18.116,0 l50.594-50.594v199.27H43.744l50.594-50.594c5.009-5.009,5.009-13.107,0-18.116c-5.009-5.009-13.107-5.009-18.116,0L3.757,246.756 c-5.009,5.001-5.009,13.107,0,18.116l72.465,72.457c5.009,5.009,13.107,5.009,18.116,0c5.009-5.001,5.009-13.107,0-18.116 l-50.594-50.594h199.458v199.646l-50.594-50.594c-5.009-5.001-13.107-5.001-18.116,0c-5.009,5.009-5.009,13.107,0,18.116 l72.457,72.465c5,5,13.107,5,18.116,0l72.465-72.457c5.009-5.009,5.009-13.107,0-18.116c-5.009-5-13.107-5-18.116,0 l-50.594,50.594V268.627h199.458l-50.594,50.594c-5.009,5.009-5.009,13.107,0,18.116s13.107,5.009,18.116,0l72.465-72.457 C513.257,259.872,513.257,251.765,508.247,246.756z"/> </g> </g> <g> </g> </svg>',
  };

  const functions = {
    position: () => {
      const e = window.event;
      const clickPosX = e.clientX;
      const clickPosY = e.clientY;
      const obj = {};

      obj.left = clickPosX;
      obj.top = clickPosY;

      return obj;
    },

    rePositioning: (picker) => {
      picker.getBoundingClientRect().right > window.screen.availWidth
        ? (picker.style.left =
            window.screen.availWidth - picker.offsetWidth + "px")
        : false;

      if (window.innerHeight > pickerHeight) {
        picker.getBoundingClientRect().bottom > window.innerHeight
          ? (picker.style.top = window.innerHeight - picker.offsetHeight + "px")
          : false;
      }
    },

    render: () => {
      categoryList = undefined;
      emojiList = undefined;

      this.insertInto = this.options.insertInto;

      const position = functions.position();

      if (!emojisHTML.length) {
        const groups = [...new Set(emojiObj.map((group) => group.category))];

        const groupedEmojis = groups.map((group) => {
          return {
            name: group,
            slug: group
              .toLowerCase()
              .replace(/[^\w]/g, "_")
              .replace(/_+/g, "_"),
            emojis: emojiObj.filter((emoji) => emoji.category === group),
          };
        });

        groupedEmojis.forEach((entry) => {
          const { name, slug, emojis } = entry;

          categoriesHTML += `<li class="flex-grow h-full flex items-center justify-center"><a title="${name}" href="#${slug}">${emojis[0].emoji}</a></li>`;
          emojisHTML += `<div class="fg-emoji-picker-category-wrapper flex flex-wrap" id="${slug}">`;
          emojisHTML += `<p class="block mt-2 p-2 pt-0 text-base font-semibold border-b flex-grow-0 flex-shrink-0 w-full bg-default">${name}</p>`;
          emojis?.forEach((ej) => {
            emojisHTML += `<li class="basis-1/6 h-12" data-tags="${ej.tags.join(
              " "
            )}" data-title="${ej.aliases[0]}">
                <a data-name="${ej.aliases[0]}" title="${
              ej.description
            }" href="#" class="w-full h-full flex justify-center items-center font-emoji focus:ring ring-offset-1 focus:ring-inset focus:outline-none ring-purple-600 emoji-button text-xl">${
              ej.emoji
            }</a>
            </li>`;
          });
          emojisHTML += "</div>";
        });
      }

      if (document.querySelector("#fg-emoji-container")) {
        this.lib("#fg-emoji-container").remove();
      }

      const picker = `
              <div class="overflow-hidden fixed top-0 left-0 shadow-lg rounded-lg border bg-default z-[9999]" id="fg-emoji-container" style="left:${
                position.left
              }px;top:${position.top}px;width:${pickerWidth}px;">
        <nav class="bg-default">
          <ul class="flex flex-wrap list-none m-0 p-0 border-b h-9">
            ${categoriesHTML}

            <li class="bg-gray-100 dark:bg-gray-900 flex-grow flex justify-center items-center h-full cursor-move" id="fg-emoji-picker-move"><a class="cursor-move" href="#">${
              icons.move
            }</a></li>
          </ul>
        </nav>

        <div class="flex h-14 relative">
          <input id="emoji-search" class="flex-grow input focus:ring-0 m-2 pr-9" type="text" placeholder="Search" autofocus />
          
          <span class="select-none pointer-events-none absolute top-0 right-0 w-14 h-14 flex items-center justify-center">${
            icons.search
          }</span>
        </div>
        <ul class="list-none m-0 p-0 overflow-y-auto h-80">
            ${emojisHTML}
        </ul>
        ${
          document.querySelector(this.insertInto).value &&
          `<div class="m-2">
          <button class="button small destructive w-full" id="clear-emoji">Remove emoji</button>
          </div>`
        }
      </div>`;

      document.body.insertAdjacentHTML("beforeend", picker);

      functions.rePositioning(document.querySelector("#fg-emoji-container"));

      setTimeout(() => {
        document.querySelector("#emoji-search").focus();
      }, 150);
    },

    closePicker: (e) => {
      e.preventDefault();

      this.lib("#fg-emoji-container").remove();

      mouseMove = false;
    },

    checkPickerExist(e) {
      if (
        document.querySelector("#fg-emoji-container") &&
        !e.target.closest("#fg-emoji-container") &&
        !mouseMove
      ) {
        functions.closePicker.call(this, e);
      }
    },

    setCaretPosition: (field, caretPos) => {
      var elem = field;
      if (elem != null) {
        if (elem.createTextRange) {
          var range = elem.createTextRange();
          range.move("character", caretPos);
          range.select();
        } else {
          if (elem.selectionStart) {
            elem.focus();
            elem.setSelectionRange(caretPos, caretPos);
          } else {
            elem.focus();
          }
        }
      }
    },

    insert: (e) => {
      e.preventDefault();

      const rawEmoji = e.target.innerHTML;
      const emoji = e.target.dataset.name.trim();
      const emojiInput = document.querySelector(this.insertInto);

      emojiInput.value = emoji;
      emojiInput.focus();

      emojiInput.dispatchEvent(new InputEvent("input"));
      functions.closePicker.call(this, e);
      document.querySelector("#emoji-display-name").innerHTML = `:${emoji}:`;
      document.querySelector("#emoji-display").innerHTML = rawEmoji;
    },

    clear: (e) => {
      e.preventDefault();

      const emojiInput = document.querySelector("#emoji-input");
      emojiInput.value = "";

      document.querySelector("#emoji-display").innerHTML = "🙂";
      document.querySelector("#emoji-display-name").innerHTML = "Add emoji";

      this.lib("#fg-emoji-container").remove();

      mouseMove = false;
    },

    search: (e) => {
      const val = e.target.value.trim().toLowerCase();

      if (!emojiList) {
        emojiList = Array.from(
          document.querySelectorAll(".fg-emoji-picker-category-wrapper li")
        );
      }

      if (!categoryList) {
        categoryList = Array.from(
          document.querySelectorAll(".fg-emoji-picker-category-wrapper")
        );
      }

      emojiList.filter((emoji) => {
        if (
          !(
            emoji.getAttribute("data-title").match(val) ||
            emoji.getAttribute("data-tags").match(val)
          )
        ) {
          emoji.style.display = "none";
        } else {
          emoji.style.display = "";
        }
      });

      categoryList.forEach((category) => {
        const emojis = Array.from(category.querySelectorAll("li"));

        if (emojis.every((emoji) => emoji.style.display === "none")) {
          category.style.display = "none";
        } else {
          category.style.display = "";
        }
      });
    },

    mouseDown: (e) => {
      e.preventDefault();
      mouseMove = true;
    },

    mouseUp: (e) => {
      e.preventDefault();
      mouseMove = false;
    },

    mouseMove: (e) => {
      if (mouseMove) {
        e.preventDefault();
        const el = document.querySelector("#fg-emoji-container");
        el.style.left = e.clientX - 320 + "px";
        el.style.top = e.clientY - 10 + "px";
      }
    },
  };

  const bindEvents = () => {
    this.lib(document.body).on(
      "click",
      functions.closePicker,
      "#fg-emoji-picker-close-button"
    );
    this.lib(document.body).on("click", functions.checkPickerExist);
    this.lib(document.body).on("click", functions.render, this.trigger);
    this.lib(document.body).on("click", functions.clear, "#clear-emoji");
    this.lib(document.body).on("click", functions.insert, "a.emoji-button");
    this.lib(document.body).on(
      "click",
      functions.categoryNav,
      ".fg-emoji-nav a"
    );
    this.lib(document.body).on("input", functions.search, "#emoji-search");
    this.lib(document).on(
      "mousedown",
      functions.mouseDown,
      "#fg-emoji-picker-move"
    );
    this.lib(document).on(
      "mouseup",
      functions.mouseUp,
      "#fg-emoji-picker-move"
    );
    this.lib(document).on("mousemove", functions.mouseMove);
  };

  (() => {
    // Event functions
    bindEvents.call(this);
  })();
};

export default EmojiPicker;
