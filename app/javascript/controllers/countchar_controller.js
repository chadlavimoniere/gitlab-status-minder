import { Controller } from "@hotwired/stimulus";
export default class extends Controller {
  static targets = ["name", "counter", "wrapper"];

  countCharacters(event) {
    const characters = this.nameTarget.value.length;
    this.counterTarget.innerText = characters;

    if (characters > 100) {
      this.wrapperTarget.classList.add("text-red-600");
    } else {
      this.wrapperTarget.classList.remove("text-red-600");
    }
  }
}
