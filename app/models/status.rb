class Status < ApplicationRecord
  def posted?
    self.done == true
  end

  def failed?
    self.failed == true
  end

  after_save_commit do
    if starts_previously_changed?
      UpdateStatusJob.set(wait_until: self.starts).perform_later(self)
    end
    if expires_previously_changed?
      ClearStatusJob.set(wait_until: self.expires).perform_later(self)
    end
  end

  GITLAB_API_URL = "https://gitlab.com/api/v4/user/status"

  def clear_status
    conn = Faraday.new(
      url: GITLAB_API_URL,
      params: {},
      headers: {'PRIVATE-TOKEN' => User.find_by(id: self.created_by).personal_access_token}
    )
    conn.put()
  end

  def patch_status
    conn = Faraday.new(
      url: GITLAB_API_URL,
      params: {
        message: self.text,
        emoji: self.emoji,
        availability: self.busy ? "busy" : "available",
      },
      headers: {'PRIVATE-TOKEN' => User.find_by(id: self.created_by).personal_access_token, 'Content-Type': 'application/json' }
    )
    conn.put()
  end

  def api_call!
    begin
      response = patch_status
    rescue Faraday::Error => e
      # You can handle errors here (4xx/5xx responses, timeouts, etc.)
      puts e.response[:status]
      puts e.response[:body]
    end
    success = response.success?
    update(done: response.success?)
    update(failed: !response.success?)
  end

  validates :expires, :allow_blank => true, comparison: { greater_than: :starts, message: "must be after start" }
  validates_length_of :text, maximum: 100
end
